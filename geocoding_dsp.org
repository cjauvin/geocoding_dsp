* Tasks

** Setup customized geocode_accidents_XXX.py script for the task     :python:
   CLOCK: [2015-11-27 Fri 15:43]--[2015-11-27 Fri 15:52] =>  0:09

** Setup geo tables
   CLOCK: [2015-12-14 Mon 09:38]--[2015-12-14 Mon 10:29] =>  0:51
   CLOCK: [2015-12-01 Tue 15:25]--[2015-12-01 Tue 16:32] =>  1:07
   CLOCK: [2015-11-29 Sun 19:17]--[2015-11-29 Sun 20:03] =>  0:46
   CLOCK: [2015-11-28 Sat 13:22]--[2015-11-28 Sat 13:48] =>  0:26
   CLOCK: [2015-11-27 Fri 15:52]--[2015-11-27 Fri 16:26] =>  0:34

** QuantumGIS
   CLOCK: [2015-12-14 Mon 10:40]--[2015-12-14 Mon 11:11] =>  0:31

* HOWTOs

** Determine srid of shapefile:                                     :postgis:
   - Paste .prj content in http://prj2epsg.org/search
   - $ shp2pgsql -d -W latin1 -s 3798:4269 AQ2014_ROUTES | psql geocoding_dsp --quiet
     here 4269 is the desired target srid
   - e.g. PROJCS["Conique_conforme_de_Lambert_du_MTQ_utilis_e_pour_Adresse_Qu_bec",GEOGCS["GCS_North_American_1983",DATUM["D_North_American_1983",SPHEROID["Geodetic_Reference_System_of_1980",6378137,298.2572221009113]],PRIMEM["Greenwich",0],UNIT["Degree",0.017453292519943295]],PROJECTION["Lambert_Conformal_Conic"],PARAMETER["standard_parallel_1",50],PARAMETER["standard_parallel_2",46],PARAMETER["latitude_of_origin",44],PARAMETER["central_meridian",-70],PARAMETER["false_easting",800000],PARAMETER["false_northing",0],UNIT["Meter",1],PARAMETER["scale_factor",1.0]]
          --> srid=3798

** AQ import

$ shp2pgsql -d -I -s 3798:4269 -W latin1 AQ2014_ROUTES aq_routes_2014 | psql geocoding_dsp --quiet

** db2 file to PG table:                                                :db2:

apt-get install pgdbf
$ pgdbf -s latin1 CJsaaq030613141516Accidents20002014.dbf | psql geocoding_dsp

* Notes

** vagrant
  - Root password of vagrant VM is "dsp"

** PostGIS VM to desktop bridge for QGIS:                          :postgres:

   - "host    all             all             10.0.2.2/32             trust" dans /etc/postgresql/9.4/main/pg_hba.conf
   - "listen_addresses = '*' dans /etc/postgresql/9.4/main/postgresql.conf
   - sudo service postgresql restart
   - $ createuser christian -s

** PG dump/restore

$ pg_dump geocoding_dsp -F c -C -f geocoding_dsp_2015-12-19.dump
$ createdb geocoding_dsp2
$ pg_restore -d geocoding_dsp2 geocoding_dsp_2015-12-19.dump

** Ajouter une colonne random à la table de données pour faire un shuffle sans avoir à utiliser "order by random()":

alter table bla add column rnd int default random() * 100000000;

** Créer csv

geocoding_dsp=# \copy (select * from cjsaaq030613141516accidents20002014_results order by num) to 'cjsaaq030613141516accidents20002014_results.csv' csv header

** Upgrade PG

https://gist.github.com/johanndt/6436bfad28c86b28f794

* Données

Proviennent de http://www.adressesquebec.gouv.qc.ca/

* 2017

** commands

python geocode_accidents.py 2011 0 1000 --delete-results | psql geocoding_dsp > /dev/null

** sql

geocoding_dsp=# \copy (select * from t_cj_acc_results order by success, result_addr, result_inter) to '/home/vagrant/christian/Desktop/bla.csv' with csv header ;
