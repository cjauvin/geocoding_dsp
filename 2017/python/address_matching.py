# -*- coding: utf-8 -*-

from __future__ import division, print_function

import sys, sqlite3, psycopg2, psycopg2.extras, unicodedata, re, argparse, codecs, locale, operator, cPickle, Levenshtein
from collections import defaultdict

from geocoding import *


# to receive unicode from db
psycopg2.extensions.register_type(psycopg2.extensions.UNICODE)
psycopg2.extensions.register_type(psycopg2.extensions.UNICODEARRAY)


def strip_accents(s):
    return ''.join((c for c in unicodedata.normalize('NFD', s) if unicodedata.category(c) != 'Mn'))


def rechunk(s):
    return ' '.join(s.split())


def normalize_source(s):
    s = re.sub(r'\W+', ' ', strip_accents(s.upper())).strip()
    s = re.sub(r'(\d+)', r'\1 ', s) # add space after digits: 100E -> 100 E
    #s = re.sub(r'\b[A-Z]\b', '', s) # remove lone letters: 100 X TOTO -> 100 TOTO
    s = re.sub(r'\bDE LA\b|\bDE L\b|\bDE\b|\bDES\b|\bDU\b|\bLE\b|\bA\b|\bLA\b|\bA LA\b|\bLES\b|\bET\b|\bAU\b', '', s)
    s = re.sub(r'\bBD\b|\bBOUL\b|\bBOULEVARD\b|\bAUT\b|\bCH\b|\bCHEMIN\b|\bAV\b|\bAVENUE\b|\bRUE\b|\bRANG\b|\bMONTEE\b|\bVOIE\b', '', s)
    s = re.sub(r'\bDOCTEUR\b|\bCURE\b', '', s)
    s = re.sub(r'\bSAINT\b|\bSAINTE\b|\bST\b|\bSTE\b', '<ST>', s).replace('<ST> ', '<ST>-')
    s = s.replace('MONSEIGNEUR', 'MGR')
    s = s.replace('RIVIERE', 'RIV')
    for d in [r'\bNORD\b', r'\bSUD\b', r'\bOUEST\b', r'\bEST\b']:
        s = re.sub(d, d[1], s)
    results = []
    # detect numerical parts and isolate them
    for m in re.finditer('\d+', s):
        num = m.group()
        non_num = (s[0:m.start()] + s[m.start()+len(num):]).strip()
        results.append(num)
        if non_num:
            results.append(rechunk(non_num))
    if results:
        return results
    return [rechunk(s)]


def parse_street_numbers(addr):
    m = re.search(r'\b[0-9][0-9 ]*\b', addr)
    if m:
        sno = m.group()
        non_sno = addr[0:m.start()] + addr[m.end():]
        sno_parts = sno.split()
        snos = list(set(sno_parts + [''.join(sno_parts)]))
        snos = sorted(snos, key=len, reverse=True) # begin by longest
        snos = map(int, snos)
        return (snos, non_sno)
    return ([], None)


def parse_street_directions(addr):
    directions = []
    for a, b in [('OUEST', 'O'), ('EST', 'E'), ('NORD', 'N'), ('SUD', 'S')]:
        if re.search(r'\b(%s|%s)\b' % (a, b), addr, re.UNICODE): # watch out for accents here, e.g. 'ALLÉE'
            directions.append(a) # to match with aq_routes_2013.gaodoorien values
    return directions

# to pickle a df that would normally use a lambda function: must be named,
# must be in __main__ namespace
def make_set_defaultdict(): return defaultdict(set)


class NgramBasedSDRStreetNameDB:

    def __init__(self, sdr_year, n):

        # sn_pair: (sn_db, sn_norm), where sn is DB's streetname field
        self.sdr_to_key_to_snids = defaultdict(make_set_defaultdict)
        self.sdr_to_nortes = defaultdict(set) # sdr -> set of norte (int)
        self.snid_to_sn_pair = {}
        self.sdr_year = sdr_year
        self.n = n

    def extract(self, cur):

        cur.execute("""
            select distinct odospeci sn, sdridu sdr
            from aq_routes
            inner join routes_sdr_{}_buffered_inter inter using (gid)
            where odospeci != ''
        """.format(self.sdr_year))

        snid_idx = 0
        sn_to_snid = {}

        while True:
            rows = cur.fetchmany()
            if not rows: break
            for row in rows:
                for sn_norm in normalize_source(row['sn']):
                    sn_norm = sn_norm.ljust(self.n) # pad with spaces in case smaller than n
                    sn_pair = (row['sn'], sn_norm.strip()) # note that here we don't want padding spaces though
                    #print ('%s -> %s' % (row['sn'], sn_norm)).encode('utf8').strip()
                    if row['sn'] not in sn_to_snid:
                        sn_to_snid[row['sn']] = snid_idx
                        self.snid_to_sn_pair[snid_idx] = sn_pair
                        snid_idx += 1
                    snid = sn_to_snid[row['sn']]
                    for i in range(len(sn_norm) - self.n + 1):
                        key = sn_norm[i:(i + self.n)]
                        self.sdr_to_key_to_snids[row['sdr']][key].add(snid)
                        #print('%s -> %s -> %s' % (row['sdr'], key, sn_pair))

        cur.execute("""
            select distinct norte, sdridu sdr
            from aq_routes
            inner join routes_sdr_{}_buffered_inter inter using (gid)
            where norte is not null
        """.format(self.sdr_year))

        for row in cur.fetchall():
            self.sdr_to_nortes[row['sdr']].add(int(row['norte']))

    # generator of (sn_db_target, sn_norm_target, is_norte)'s
    def query(self, sn_norm, sdr):

        if sn_norm.isdigit() and int(sn_norm) in self.sdr_to_nortes[sdr]:
            norte = int(sn_norm)
            yield norte, norte, True
        else:
            sn_norm = sn_norm.ljust(self.n) # pad so that it has len n
            for i in range(len(sn_norm) - self.n + 1):
                key = sn_norm[i:(i + self.n)]
                for snid in self.sdr_to_key_to_snids[sdr][key]:
                    pair = self.snid_to_sn_pair[snid]
                    yield pair[0], pair[1], False

    @classmethod
    def load(cls, fn):
        return cPickle.load(open(fn, 'rb')) # highest protocol, must be read/write in binary mode

    def save(self, fn):
        with open(fn, 'wb') as f:
            cPickle.dump(self, f, -1)


class AddressMatcher:

    def __init__(self, cur,
                 sdr_year, n=3,
                 exact_match_cutoff=5,
                 sim_threshold=0.8,
                 max_inter_bb_radius_in_meters=150,
                 extract=False):

        self.exact_match_cutoff = exact_match_cutoff
        self.sim_threshold = sim_threshold
        self.max_inter_bb_radius_in_meters = max_inter_bb_radius_in_meters
        if extract:
            print('Extracting/saving SDR_%s streetname database..' % sdr_year, file=sys.stderr, end=' ')
            self.db = NgramBasedSDRStreetNameDB(sdr_year, n)
            self.db.extract(cur)
            self.db.save('streetname_db_type=ngram_sdr=%s_sn=%s.pkl' % (sdr_year, STREETNAME_FIELD))
        else:
            fn = '../data/db_sdr=%s_n=%s.pkl' % (sdr_year, n)
            print('Loading %s..' % fn, file=sys.stderr, end=' ')
            self.db = NgramBasedSDRStreetNameDB.load(fn)
        print('done', file=sys.stderr)
        self.n_match_calls = 0
        self.n_match_trials = 0
        self.n_geocoding_cache_hits = 0
        self.geocoding_cache = {} # geocoding: set(sno, sn_target, regional_constraint) -> point
                                  # intersection: set() -> point
        self.inter_table = 'routes_sdr_%s_buffered_inter' % sdr_year
        self.sdr_year = sdr_year

    def match_address(self, cur, source1, source2, sdr):

        self.n_match_calls += 1
        results = []
        dirs_source1 = parse_street_directions(source1) + [None]
        snos_source1, nonsno_source1 = parse_street_numbers(source1)
        for sno_source1 in snos_source1:
            for norm_source1 in normalize_source(nonsno_source1):
                for sn_norm_source1, sn_norm_target1, sn_db_target1, sim1 in self._street_name_parsing_iter(norm_source1, sdr):
                    for dir_source1 in dirs_source1:
                        self.n_match_trials += 1
                        key = (sno_source1, sn_db_target1, dir_source1, sdr)
                        if key in self.geocoding_cache:
                            geo = self.geocoding_cache[key]
                            self.n_geocoding_cache_hits += 1
                        else:
                            # geo is a list
                            geo = find_address(
                                cur, sno_source1, sn_db_target1,
                                dir_source1, sdr, self.sdr_year
                            )
                            # geo = geocode(
                            #     cur, sno_source1,
                            #     sn_db_target1,
                            #     dir_source1, sdr,
                            #     self.inter_table
                            # )
                        self.geocoding_cache[key] = geo
                        if not geo:
                            continue
                        target1 = '%s %s%s' % (sno_source1, sn_db_target1, ' ' + dir_source1 if dir_source1 else '')
                        result = {
                            'sim1': sim1,
                            'lev1': Levenshtein.distance(str(sn_norm_source1), str(sn_norm_target1)),
                            'sno_source1': sno_source1,
                            'sn_norm_source1': sn_norm_source1,
                            'sn_norm_target1': sn_norm_target1,
                            'sn_db_target1': sn_db_target1,
                            'target1': target1,
                            'dir_source1': dir_source1,
                            'geo': geo
                        }
                        if sim1 == 1 and len(geo) == 1: # cannot do better, stop search
                            return result
                        results.append(result)
        best = {}
        if results:
            best = max(results, key=lambda r: r['sim1'])
            if len(best['geo']) > 1: # ambiguous, try to disambiguate with source2
                for norm_source2 in normalize_source(source2):
                    for sn_norm_source2, sn_norm_target2, sn_db_target2, sim2 in self._street_name_parsing_iter(norm_source2, sdr):
                        gid = disambiguate_with_intersection(
                            cur,
                            ambiguous_gids=[r['gid'] for r in best['geo']],
                            inter_sn=sn_db_target2,
                            sdr=sdr,
                            inter_table=self.inter_table
                        )
                        if gid:
                            #best['geo'] = {gid: best['geo'][gid]}
                            coords = next(r['coords'] for r in best['geo'] if r['gid'] == gid)
                            best['geo'] = [{'coords': coords, 'gid': gid}]
                            best['result_addr'] = 'yes_with_source2'
                            return best
        return best

    def match_intersection(self, cur, source1, source2, sdr):

        #if not source1 or not source2: return None
        self.n_match_calls += 1
        results = []
        for norm_source1 in normalize_source(source1):
            for norm_source2 in normalize_source(source2):
                for sn_norm_source1, sn_norm_target1, sn_db_target1, sim1 in self._street_name_parsing_iter(norm_source1, sdr):
                    for sn_norm_source2, sn_norm_target2, sn_db_target2, sim2 in self._street_name_parsing_iter(norm_source2, sdr):
                        self.n_match_trials += 1
                        key = (sn_db_target1, sn_db_target2, sdr)
                        if key in self.geocoding_cache:
                            geo = self.geocoding_cache[key]
                        else:
                            geo = intersect(
                                cur, sn_db_target1,
                                sn_db_target2, sdr,
                                self.inter_table
                            )
                            self.geocoding_cache[key] = geo
                        if not geo: continue
                        result = {
                            'sim1': sim1,
                            'sim2': sim2,
                            'lev1': Levenshtein.distance(str(sn_norm_source1), str(sn_norm_target1)),
                            'lev2': Levenshtein.distance(str(sn_norm_source2), str(sn_norm_target2)),
                            'sn_norm_source1': sn_norm_source1,
                            'sn_norm_target1': sn_norm_target1,
                            'sn_norm_source2': sn_norm_source2,
                            'sn_norm_target2': sn_norm_target2,
                            'target1': sn_db_target1,
                            'target2': sn_db_target2,
                            'result_inter': 'ambiguous' if geo['bb_radius'] > self.max_inter_bb_radius_in_meters else 'yes',
                            'inter_bb_radius': geo['bb_radius'],
                            'geo': geo
                        }
                        if sim1 == 1 and sim2 == 1 and result['result_inter'] != 'ambiguous':
                            return result # cannot do better, return right away
                        results.append(result)
        if results:
            return max(results, key=lambda d: (d['sim1'], d['sim2']))
        return {}

    def match_intersection_from_addr_result(self, cur, addr_result, source2, sdr):

        self.n_match_calls += 1
        results = []
        sn_db_target1 = addr_result['sn_db_target1']
        for norm_source2 in normalize_source(source2):
            for sn_norm_source2, sn_norm_target2, sn_db_target2, sim2 in self._street_name_parsing_iter(norm_source2, sdr):
                if sn_db_target1 == sn_db_target2: continue
                self.n_match_trials += 1
                key = (sn_db_target1, sn_db_target2, sdr)
                if key in self.geocoding_cache:
                    geo = self.geocoding_cache[key]
                else:
                    geo = intersect(
                        cur, sn_db_target1,
                        sn_db_target2, sdr,
                        self.inter_table
                    )
                    self.geocoding_cache[key] = geo
                if not geo:
                    continue
                result = {
                    'sim2': sim2,
                    'lev2': Levenshtein.distance(str(sn_norm_source2), str(sn_norm_target2)),
                    'sn_norm_source2': sn_norm_source2,
                    'sn_norm_target2': sn_norm_target2,
                    #'target1': sn_db_target1,
                    'target2': sn_db_target2,
                    'result_inter': 'ambiguous' if geo['bb_radius'] > self.max_inter_bb_radius_in_meters else 'yes_with_source1',
                    'inter_bb_radius': geo['bb_radius'],
                    'geo': geo
                }
                if sim2 == 1 and result['result_inter'] != 'ambiguous':
                    return result # cannot do better, return right away
                results.append(result)
        if results:
            return max(results, key=lambda d: d['sim2'])
        return {}

    def _street_name_parsing_iter(self, source_norm, sdr):

        source_parts = source_norm.split()
        n_source_parts = len(source_parts)
        for n in range(n_source_parts, 0, -1):
            for p in range(n_source_parts - n + 1):
                sn_norm_source = ' '.join(source_parts[p:p+n])
                for sn_db_target, sn_norm_target, is_norte in self.db.query(sn_norm_source, sdr):
                    if is_norte:
                        sim = 1
                        sim_and_long_enough = True
                    else:
                        sim = Levenshtein.ratio(sn_norm_source, sn_norm_target)
                        sim_and_long_enough = len(sn_db_target) >= self.exact_match_cutoff and sim >= self.sim_threshold
                    if sim == 1 or sim_and_long_enough:
                        yield sn_norm_source, sn_norm_target, sn_db_target, sim


if __name__ == '__main__':

    parser = argparse.ArgumentParser(description='')
    parser.add_argument('sdr_year', help='')
    parser.add_argument('--addr', help='')
    parser.add_argument('--inter', help='', nargs=2)
    parser.add_argument('--sim_threshold', help='similarity threshold value (default 0.8)', type=float, default=0.8)
    parser.add_argument('--enforce_single_inter_point', help='default False', action='store_true')
    parser.add_argument('--exact_match_cutoff', help='any name smaller than this must match exactly (default 5)', type=int, default=5)
    parser.add_argument('--debug', help='default False', action='store_true')
    parser.add_argument('--sdr')
    extract_group = parser.add_argument_group('streetname DB extraction')
    extract_group.add_argument('--extract', help='will extract a new streetname DB (and store it as a pkl file)', action='store_true', default=False)
    extract_group.add_argument('--ngram', help='ngram order; default 3', type=int, default=3)
    try: args = parser.parse_args()
    except IOError, msg: parser.error(str(msg))

    if args.addr: args.addr = args.addr.decode('utf8')
    if args.inter: args.inter = [s.decode('utf8') for s in args.inter]

    conn = psycopg2.connect("dbname=geocoding_dsp")
    cur = conn.cursor(cursor_factory=psycopg2.extras.RealDictCursor)

    if args.extract:

        db = NgramBasedSDRStreetNameDB(args.sdr_year, args.ngram)
        db.extract(cur)
        db.save('../data/db_sdr=%s_n=%s.pkl' % (args.sdr_year, args.ngram))

    else:
        matcher = AddressMatcher(cur, args.sdr_year)
        if args.addr:
            print(matcher.match_address(cur, args.addr, '', args.sdr))
        if args.inter:
            print(matcher.matchIntersection(args.inter[0], args.inter[1], args.regional_constraint))

    conn.close()
