# -*- coding: utf-8 -*-

from __future__ import division, print_function

import psycopg2
import psycopg2.extras
import re
import sys
import argparse
from little_pger import LittlePGer

from address_matching import *


def insert(table, values):
    global result_cols, pg
    values = dict([(c, v) for c, v in values.items() if c in result_cols])
    q = "insert into {tbl} ({cols}) values ({vals})".format(
        tbl=table,
        cols=','.join(values.keys()),
        vals=','.join(['%s' for v in values])
    )
    return pg.cursor.mogrify(q, values.values()) + ';'


# to receive unicode from db
psycopg2.extensions.register_type(psycopg2.extensions.UNICODE)
psycopg2.extensions.register_type(psycopg2.extensions.UNICODEARRAY)

parser = argparse.ArgumentParser(description='')
parser.add_argument('sdr_year')
parser.add_argument('slice', type=int)
parser.add_argument('slice_size', type=int)
parser.add_argument('--sim-threshold',
                    help='similarity threshold value (default 0.70)',
                    type=float, default=0.70)
parser.add_argument('--enforce-single-inter-point',
                    help='default False (i.e. less restrictive)',
                    action='store_true')
parser.add_argument('--exact-match-cutoff',
                    help=('any name smaller than this must match '
                          'exactly (default 5)'), type=int, default=5)
parser.add_argument('--n', help='ngram order; default 3', type=int, default=3)
parser.add_argument('--extract', help='default False', action='store_true')
parser.add_argument('--id', help='default None', type=int)
parser.add_argument('--delete-results', help='default False',
                    action='store_true')
try:
    args = parser.parse_args()
except IOError, msg:
    parser.error(str(msg))

pg = LittlePGer(conn='dbname=geocoding_dsp', commit=True)

input_table = 't_cj_acc'
result_table = input_table + '_results_adresses'

result_fields = [
    ('num',             'int primary key'),
    ('success',         'bool default false'),
    ('result_addr',     "text default 'no'"),
    ('result_inter',    "text default 'no'"),
    ('sdr',             'text'),
    ('sdr_year',        'text'),
    ('source1',         'text'),
    ('source2',         'text'),
    ('sno_source1',     'int'),
    ('sn_norm_source1', 'text'),
    ('sn_norm_target1', 'text'),
    ('dir_source1',     'text'),
    ('target1',         'text'),
    ('sim1',            'float'),
    ('lev1',            'int'),
    ('sn_norm_source2', 'text'),
    ('sn_norm_target2', 'text'),
    ('target2',         'text'),
    ('sim2',            'float'),
    ('lev2',            'int'),
    ('lat_addr',        'float'),
    ('long_addr',       'float'),
    ('lat_inter',       'float'),
    ('long_inter',      'float'),
    ('addr_inter_dist', 'float'),
    ('inter_bb_radius', 'float')
]
result_cols = [f[0] for f in result_fields]

if args.delete_results:
    pg.sql('drop table if exists {tbl}; create table {tbl} ({fields})'.format(
        tbl=result_table,
        fields=','.join(['%s %s' % (f, t) for f, t in result_fields])
    ))
    pg.commit()

matcher = AddressMatcher(pg.cursor, args.sdr_year, extract=args.extract)

if args.extract:
    exit()

cache = {}  # (source1, source2, sdr) -> result
n_cache_hits = 0

where = {'carto': 'sdr%s_SAAQ' % args.sdr_year}
if args.id is not None:
    where['idnum'] = str(args.id)

for i, row in enumerate(pg.select(
        input_table,
        where=where,
        offset=(args.slice * args.slice_size),
        limit=args.slice_size,
        order_by='rnd')):

    if i % 1000 == 0:
        print(i, file=sys.stderr)

    seq, sdr, _, source1, source2 = [
        unicode(row[f]) for f in ['idnum', 'sdridu', 'carto', 'rueaccdnpl', 'accdn_pres']
    ]
    seq = int(float(seq))
    source1 = source1.strip()
    source2 = source2.strip()

    if not source1 and source2:
        parts = source2.split(' ET ')
        if len(parts) == 2:
            source1 = parts[0].strip()
            source2 = parts[1].strip()

    # cache
    result = cache.get((source1, source2, sdr))
    if result:
        result['num'] = seq
        print(insert(result_table, values=result))
        n_cache_hits += 1
        continue

    # single address (found in source1)
    result = matcher.match_address(pg.cursor, source1, source2, sdr)

    if result:
        result['num'] = seq
        if len(result['geo']) == 1:
            lon, lat = re.match('POINT\((.*) (.*)\)', result['geo'][0]['coords']).groups()
            result['long_addr'] = lon
            result['lat_addr'] = lat
            result['result_addr'] = result.get('result_addr', 'yes')
            #result['addr_gid'] = result['geo'].iterkeys().next()
        elif len(result['geo']) > 1:
            result['result_addr'] = 'ambiguous'
        else:
            assert False
        result['success'] = result['result_addr'] != 'ambiguous' # bool
        result['sdr'] = sdr
        result['sdr_year'] = args.sdr_year
        result['source1'] = source1
        result['source2'] = source2
        result2 = matcher.match_intersection_from_addr_result(pg.cursor, result, source2, sdr)
        if result2:
            # result['success'] = result2['result_inter'] == 'yes'
            # result['result_inter'] = 'yes_with_source1'
            lon, lat = re.match('POINT\((.*) (.*)\)', result2['geo']['coords']).groups()
            result['long_inter'] = lon
            result['lat_inter'] = lat
            if (result['result_addr'].startswith('yes') and
                result2['result_inter'].startswith('yes')):  # i.e. not ambig
                pg.cursor.execute("""
                     select st_distance(
                         st_geomfromtext(%s)::geography,
                         st_geomfromtext(%s)::geography
                     ) as dist
                """, [result['geo'][0]['coords'], result2['geo']['coords']])
                result['addr_inter_dist'] = pg.cursor.fetchone()['dist']
            result.update(result2)
        print(insert(result_table, result))
        cache[(source1, source2, sdr)] = result
        continue

    # intersection source1/source2
    result = matcher.match_intersection(pg.cursor, source1, source2, sdr)
    if result:
        result['num'] = seq
        #print result
        lon, lat = re.match('POINT\((.*) (.*)\)', result['geo']['coords']).groups()
        result['long_inter'] = lon
        result['lat_inter'] = lat
        result['success'] = result['result_inter'] == 'yes'
        result['sdr'] = sdr
        result['sdr_year'] = args.sdr_year
        result['source1'] = source1
        result['source2'] = source2
        print(insert(result_table, result))
        cache[(source1, source2, sdr)] = result
        continue

    # nothing found
    result = {'num': seq, 'sdr': sdr, 'sdr_year': args.sdr_year, 'source1': source1, 'source2': source2}
    cache[(source1, source2, sdr)] = result

    print(insert(result_table, result))
