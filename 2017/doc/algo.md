# Algorithme de correction et de validation d'adresses provenant de la SAAQ

## Grandes lignes de la méthode

Les données provenant de la SAAQ sont sous la forme d'une table dont
on se sert des champs suivants :

* ID: identifiant de l'entrée, correspondant à un événement particulier
* ID de SDR: identifiant d'une région de recensement, correspondant à
  une région polygonale dans un [shapefile](https://fr.wikipedia.org/wiki/Shapefile)
* Année SDR: découpage annuel particulier
  en
  [régions de recensement](http://www12.statcan.gc.ca/census-recensement/2011/ref/dict/geo012-fra.cfm) (chaque
  année correspond à un shapefile)
* Adresse 1 (`source1`): champ textuel pouvant contenir une adresse (ou
  seulement un nom de rue)
* Adresse 2 (`source2`): champ textuel pouvant contenir un nom de rue
* Numéro de route: champ numérique pouvant contenir un numéro de route

Le but de la méthode est de fournir des coordonnées géographiques
exactes (latitude et longitude) pour chaque entrée de la table
provenant de la SAAQ (correspondant à un accident routier
particulier). L'algorithme est écrit en Python, et se base sur des
données importées dans une base de données PostgresSQL, constituées
des tables suivantes :

* Une table d'adresses (i.e. des points) provenant
  de [Adresses Québec](http://adressesquebec.gouv.qc.ca/) (AQ)
* Une table du réseau routier (i.e. des lignes) provenant de AQ
* Une table de
  régions
  [SDR](http://www12.statcan.gc.ca/census-recensement/2011/ref/dict/geo012-fra.cfm) (i.e. des
  polygones) provenant de StatCan

Les difficultés particulières auxquelles cet algorithme doit faire
face sont :

* Absence de codes postaux dans les données d'entrée (ce qui
  permettrait de désambiguiser et de rendre plus efficace les
  recherches)
* Beaucoup d'imprécision et d'erreurs dans l'orthographe des noms et
  indications (aucun mécanisme de standardisation des entrées n'ayant
  été utilisé à la saisie)

Il est à noter que certains aspects plus spécifiques de la méthode
(comme le traitement des numéros de route, ou la désambiguisation
d'une adresse à l'aide d'une seconde rue), ne seront pas traités dans
ce texte, pour des besoins de simplification.

## Description détaillée de l'algorithme

### Préparation des données

#### Pré-calcul des données géographiques

La première étape consiste en la préparation des données et la
création d'une base de données PostgreSQL. Les données d'entrée sont
tout d'abord converties du format DBF vers une table SQL, à l'aide du
programme `pgdbf`. Plusieurs shapefiles provenant de AQ sont ensuite
convertis en tables SQL à l'aide de l'utilitaire `shp2pgsql`. Les
shapefiles de régions SDR sont ensuite importés d'une manière
similaire, pour les années de référence. Les régions SDR sont ensuite
"augmentées" à l'aide de l'opérateur `st_buffer` de PostGIS, de
manière à créer une "zone de tolérance" pour des adresses qui seraient
légèrement en-dehors de la zone de recensement spécifiée. Une fois ces
régions augmentées disponibles, les intersections avec les adresses et
les routes sont pré-calculées à l'aide des opérateurs `st_contains` et
`st_intersects`. Les tables SQL résultantes sont donc :

* `saaq_adresses`
* `aq_adresses`
* `aq_routes`
* `routes\_sdr\_inter\_YYYY`
* `sdr\_YYYY`

#### Fonction de normalisation des noms de rue

Nous avons ensuite besoin d'une fonction de normalisation des noms de
rue (que nous nommerons `norm`), dont le but est de réduire les
variations orthographiques de manière à simplifier les
comparaisons. Cette fonction prend en entrée une chaine de caractères
(correspondant à un nom de rue quelconque), et la transforme selon un
ensemble de règles précises. Voici quelques exemples :

```
norm("Mozart") -> "MOZART"                   # majuscules
norm("Bélanger") -> "BELANGER"               # accents
norm("Saint-Laurent") -> "<ST>-LAURENT"      # saint/sainte
norm("Sainte Catherine") -> "<ST>-CATHERINE"
norm("De la Tamise") -> "TAMISE"             # particules
```

#### Table associant les noms de rue à leurs n-grammes

Un [n-gramme](https://fr.wikipedia.org/wiki/N-gramme) est une
sous-séquence de `n` caractères contigus provenant d'une chaine de
caractères quelconque. Pour le mot "adresse" par exemple, la suite de
3-grammes qu'on peut en extraire est :

```
["adr", "dre", "res", "ess", "sse"]
```

On construit ensuite une table qui associe les n-grammes qu'on peut
extraire des noms de rue de la base de données de référence
(i.e. contenant les "vrais" noms de rue) aux noms de rue
eux-mêmes. Pour la rue "Du Rivage" et une valeur de `n=3`, la table
(appelons-la `G`) serait constituée par exemple des associations
suivantes :

```
"RIV" -> "RIVAGE"
"IVA" -> "RIVAGE"
"VAG" -> "RIVAGE"
"AGE" -> "RIVAGE"
```

Étant donné que plusieurs noms correspondront à un même n-gramme (p.ex. le
nom `"RIVIERE BLEUE"` sera lui aussi associé à `"RIV"`), les associations sont
multiples :

```
G("RIV") -> ["RIVAGE", "RIVIERE BLEUE", ...]
G("IVA") -> ["RIVAGE", "IVANKA", ...]
...
```

Comme il sera démontré plus loin, l'utilisation des n-grammes va
permettre de faire des recherches efficaces tout en offrant un certain
niveau de tolérance pour l'orthographe des noms de rue, ce qui
constitue un des éléments essentiels de cette méthode.

### Algorithme

Pour chaque entrée dans la table d'adresses de la SAAQ
(`saaq_adresses`), nous allons tenter d'en dériver des coordonnées
géographiques précises, dans un premier temps en tant qu'adresse (à
l'aide du contenu de `source1`), ou ensuite en tant qu'intersection de
deux rues (à l'aide du contenu combiné de `source1` et `source2`),
s'il n'est pas possible de trouver une adresse.

#### Recherche en tant qu'adresse

##### Recherche dans l'espace des noms de rue potentiels

La première étape de l'algorithme consiste en l'extraction, à l'aide
d'une expression régulière, des valeurs numériques du premier champ
d'adresse (`source1`), constituant des numéros d'adresses civiques
potentiels. La partie non-numérique restante du champ d'adresse
(pouvant donc contenir un nom de rue potentiel) est ensuite passée à
une fonction qui retourne une série de
4-[uplets](https://fr.wikipedia.org/wiki/N-uplet) ("tuples", ou
"structures") composés des éléments suivants :

1. Nom de rue candidat (i.e. potentiel) normalisé, provenant des
   données SAAQ (`cand_norm`)
2. Nom de rue réel normalisé, provenant des données de référence
   (`ref_norm`)
3. Nom de rue réel non-normalisé, provenant des données de référence
   (`ref_exact`)
4. Similarité (en terme de
   la
   [distance de Levenshtein](https://fr.wikipedia.org/wiki/Distance_de_Levenshtein),
   ou d'édition) entre (1) et (2) (`norm_sim`)

La série de ces 4-uplets est déterminée à partir de la décomposition
des éléments (les "mots", ou "tokens") composant la partie
non-numérique de la chaine d'entrée (`source1` moins les tokens
numériques), en étant guidé par l'heuristique selon laquelle les
"matchs" plus longs, donc plus spécifiques, devraient se voir accorder
une plus haute priorité.

Examinons un exemple spécifique : supposons que la chaine d'entrée soit

```
"123 ch. de la côte Sainte Caterine, Montréal"
```

et supposons aussi que la base de données de référence contienne ces deux
adresses réelles :

1. `"123 Côte Sainte-Catherine"`
2. `"123 Sainte-Catherine"`

Il est important tout d'abord de remarquer qu'il y a une coquille dans
le mot "Cat[h]erine". La première étape consistera simplement en la
décomposition de la chaine d'entrée en deux segments :

1. `"123"`
2. `"ch. de la côte Sainte Caterine, Montréal"`

(1) correspond à une chaine dans laquelle on cherchera un numéro
d'adresse, et (2) à une chaine dans laquelle on cherchera un nom de
rue correspondant, (1) et (2) formant donc une adresse valide, donc
réelle. Un problème potentiel que nous aimerions éviter dans ce cas
particulier serait de considérer la deuxième adresse ("123
Sainte-Catherine") "meilleure" que la deuxième ("123 Côte
Sainte-Catherine"), étant donné que cette dernière est clairement plus
appropriée.

Nous sommes à la recherche d'une sous-séquence de "mots"
(i.e. sous-chaines séparées par des espaces) composant un nom de rue
valide, et nous avons donc besoin d'une procédure qui énumère toutes
les sous-séquences tirées de la chaine d'entrée normalisée. La chaine
d'entrée, une fois normalisée, étant composée de seulement trois
"mots" (voir
les
[règles de normalisation](#fonction-de-normalisation-des-noms-de-rue)
expliquées plus haut) :

```
["COTE", "<ST>-CATERINE", "MONTREAL"]
```

L'algorithme d'énumération produira donc, à partir de ces trois tokens,
la liste de six sous-séquences suivante :

```
["COTE", "<ST>-CATERINE", "MONTREAL"]
["COTE", "<ST>-CATERINE"]               (*)
["<ST>-CATERINE", "MONTREAL"]
["COTE"]
["<ST>-CATERINE"]
["MONTREAL"]
```

Pour chacune des sous-séquences de longueur variable (en terme de
nombre de tokens), nous allons utiliser
notre
[table de n-grammes](#table-associant-les-noms-de-rue-à-leurs-n-grammes) afin
de chercher efficacement des noms de rue candidats dans la base de
données de référence, avec lequels nous allons effectuer une
comparaison (en terme de niveau de ressemblance des chaines de
caractères, ce qui est quantifiable précisément avec la notion
de
[distance de Levenshtein](https://fr.wikipedia.org/wiki/Distance_de_Levenshtein),
ou d'édition). Si un nom de rue réel est trouvé suffisamment similaire
au nom candidat, nous pousserons plus loin la recherche (en incluant
le numéro civique, comme nous verrons bientôt). Dans le cas contraire,
nous ne voudrons pas gaspiller des ressources, et il sera simplement
ignoré.

Supposons que nous en sommes à traiter la deuxième sous-séquence
(marquée d'une `*` ci-haut), correspondant au nom de rue candidat :

```
"COTE <ST>-CATERINE"
```

les 3-grammes la composant permettront de retrouver une multitude de noms
de rue réels associés (dont celui souhaité, en plusieurs "copies") :

```
 3-grammes     noms de rue réels (normalisés) associés
-----------------------------------------------------------
G("COT")   -> ["COTE <ST>-LUC", "COTE <ST>-CATHERINE", ...]
G("OTE")   -> [..., "COTE <ST>-CATHERINE"]
...
G("ATE")   -> [...]
G("TER")   -> [...]
...
G("INE")   -> [..., "MARTINE", "COTE <ST>-CATHERINE"]
```

Il est à noter que `"ATE"` et `"TER"` ne seront _pas_ associés à
`"COTE <ST>-CATHERINE"` car ils sont les 3-grammes "fautifs" (auxquels
il manque un "H"). Le bon nom sera tout de même trouvé étant donné la
tolérance et la redondance offertes par la méthode. Dans un contexte où
on n'aurait pas à se soucier de l'efficacité d'un tel algorithme de
recherche, il à noter qu'on pourrait se passer de la méthode des
n-grammes simplement en considérant, pour _chaque_ sous-séquence,
_tous_ les noms de la base de données de référence. La méthode des
n-grammes n'est utilisée que pour accélérer la recherche, en coupant
dans l'espace des possibilités (qui serait gigantesque sans cette
optimisation). Notons que les facteurs de tolérance à l'orthographe et
d'efficacité sont en opposition : l'utilisation de 4-grammes serait plus
efficace que celle de 3-grammes (car moins de possibiliés de
comparaison seraient générées), tout en augmentant les chances qu'une
coquille ne soit pas "couverte" par la redondance.

Cet algorithme de recherche produira une série de 4-uplets :

```
{
    cand_norm = "COTE <ST>-CATERINE",
    ref_norm = "COTE <ST>-LUC",
    ref_exact = "Côte Saint-Luc",
    norm_sim = 0.71
}, ..., {
    cand_norm = "COTE <ST>-CATERINE",
    ref_norm = "COTE <ST>-CATHERINE",
    ref_exact = "Côte Sainte-Catherine",
    norm_sim = 0.97
}, ..., {
    card_norm = "<ST>-CATERINE",
    ref_norm = "<ST>-CATHERINE",
    ref_exact = "Sainte-Catherine",
    norm_sim = 0.96
}, ...
```

Notons que le problème de devoir distinguer entre les deux options
valides ("Côte Sainte-Catherine" et "Sainte-Catherine") dans notre
example est solutionné par deux caractérisques de l'algorithme :

1. L'utilisation du _ratio_ de Levenshtein (au lieu de la _distance_)
   pour mesurer la similarité fait en sorte de privilégier la chaine
   la plus longue (plus spécifique)
2. Sans la coquille, les deux similarités seraient de 1, mais étant donné
   que l'énumération des sous-séquences est faite dans l'ordre décroissant de
   leur longueur, et que la recherche est arrêtée dès qu'un match parfait est
   trouvé (il ne servirait à rien de poursuivre la recherche jusqu'au bout),
   le candidat le plus spécifique serait encore une fois privilégié.

##### Recherche dans l'espace des adresses valides

Pour chaque 4-uplet, une recherche sera faite dans la base de données
de référence pour tenter de trouver une adresse réelle à l'aide du
numéro civique (extrait à l'aide d'une expression régulière comme nous
l'avons vu) et un nom de rue qu'on sait existant dans la base de
données de référence (correspondant aux champs `ref_exact` dans la
structure des 4-uplets décrite si haut). Pour chaque 4-uplet, la
recherche effectuée correspond essentiellement à la requête SQL
suivante :

```
select geom
from aq_adresses
where  no_civique = <no civique>
and    nom_rue    = <ref_exact>
and    sdr        = <sdr>
```

On voit que le champ `sdr`, qui correspond comme on l'a vu à une
région polygonale de recensement, permet à la fois de réduire l'espace
de recherche et de désambiguiser les résultats (d'une manière
similaire à ce que le code postal permettrait, s'il était
disponible). En effet, étant donné qu'il existe plusieurs rues
"Saint-Laurent" au Québec, cette précison est dans bien des cas
essentielle. Il est à noter que l'ambiguité des résultats est dans
certains cas inévitable, et est rapportée comme telle dans les
résultats, s'il n'est pas possible de faire mieux.

##### Recherche (optionnelle) dans l'espace des segments de rue valides

Une autre option pour la recherche d'une adresse valide est d'utiliser
le "géocoding", qui consiste en la recherche d'une adresse à l'aide
d'une structure de données décrivant la géométrie des segments de rue
(i.e. la table `aq_routes`), au lieu des adresses en tant que points
(la table `aq_adresses`) de
la
[section précédente](#recherche-dans-lespace-des-adresses-valides). Si
on connait en effet l'ensemble des segments composant le tracé d'une
rue, ainsi que les adresses de leurs extrémités (paires et impaires,
typiquement), on peut utiliser l'interpolation pour déterminer les
coordonnées exactes d'une adresse. Bien qu'elle permette d'exprimer la
réalité géographique d'une manière plus compacte (qu'une simple table
de points), cette technique présente néanmoins le désavantage d'être
computationnellement beaucoup plus demandante. PostGIS est
suffisamment puissant pour permettre d'implémenter la technique
entièrement en SQL (ce qui est assez remarquable en soi) mais étant
donné que la requête correspondante est passablement complexe, nous
n'allons pas la décrire ici.

Une adresse sera considérée existante si la recherche à partir des
éléments d'un 4-uplet particulier ainsi que le numéro civique est
fructeuse, c'est-à-dire qu'on a réussi à trouver des coordonnées
géographiques à l'aide de l'une des deux techniques précédemment
décrites. Le 4-uplet offrant le meilleur ratio de Levenshtein entre le
nom de rue candidat (provenant des données d'entrée) et réel
(provenant de la base de données de référence) est celui que l'on
considéra comme le résultat souhaité. Dans les cas où la requête SQL
retournera plus d'un ensemble de coordonnées, on considéra le résultat
comme étant ambigu (et il sera marqué comme tel dans les résultats en
sortie). Une tentative de désambiguisation à l'aide du deuxième champ
de nom de rue est alors tentée, mais la technique ne sera pas décrite
ici.

#### Recherche en tant qu'intersection

La recherche d'une adresse (et donc de coordonnées géographiques précises) à
l'aide du premier champ peut ne pas avoir réussi pour deux raisons :

1. Le champ ne contenait pas de numéro civique (il contenait
   seulement un nom de rue par exemple)
2. Le champ contenait un numéro civique mais les noms de rue
   considérés n'atteignait une valeur de similarité minimum pour la
   comparaison avec les noms de rue de référence

Dans le premier cas, nous allons décrire dans cette section comment le
deuxième champ est utilisé pour tenter de trouver les coordonnées
géographiques de l'intersection entre deux rues (se trouvant
respectivement dans le premier et le second champ, `source1` et
`source2`).

L'algorithme de recherche d'une intersection valide utilise la même
procédure de génération de 4-uplets décrit dans une
précédente
[section](#recherche-dans-lespace-des-noms-de-rue-potentiels), à la
différence que nous considérons maintenant le produit cartésien de
l'ensemble des 4-uplets pour la première source (i.e. premier nom de
rue potentiel) avec ceux de la deuxième source (deuxième noms de rue
potentiel). Si le premier champ génére un ensemble de trois 4-uplets
et que le deuxième en génére deux, nous considérerons donc un ensemble
de six possibilités. Pour chacune des paires de 4-uplet, nous allons
utiliser une requête PostGIS (donc SQL) pour tenter de déterminer si
elle correspond à une intersection valide :

```
select st_intersection(r1.geom, r2.geom)
from aq_routes r1, aq_routes r2
where r1.nom_rue = <ref_exact1>
and   r2.nom_rue = <ref_exact2>
and   r1.sdr     = <sdr>
and   r2.sdr     = <sdr>
```

Il est à noter que ceci est une schématisation de la requête réelle,
qui est beaucoup plus complexe car elle introduit de nombreux
paramètres supplémentaires qui nous permettront de contrôler la
qualité et la pertinence des résultats, en particulier le fait que
pour les intersections complexes (par exemple entre deux autoroutes),
nous allons considérer la taille de la "bounding box" qui enveloppe
l'intersection, afin de marquer en tant qu'"ambigus" les résultats
couvrant une superficie trop vaste. On note également que les données
de référence utilisées pour la recherche d'intersection sont
`aq_routes` (et non `aq_adresses`), une table qui contient des
segments de rue (mentionnée dans
la
[section](#recherche-optionnelle-dans-lespace-des-segments-de-rue-valides) sur
la géocoding optionel), et dont on doit avoir préalablement calculé
les intersections avec les régions polygonales SDR à l'aide des tables
`routes_sdr_inter_YYYY`. L'intersection retenue est celle offrant la
plus grande similarité pour les deux noms de rue impliqués.

### Données de sortie

Voici tous les champs retournés pour les données de sortie :

* `num`: ID de l'entrée
* `success`: `yes`/`no` avec `yes` si `result_addr` ou `result_inter` est `yes`
* `result_addr`: `{yes, no, ambiguous, yes_with_source2}`
* `result_inter`: `{yes, no, ambiguous, yes_with_source1}`
* `sdr`: ID de SDR
* `sdr_year`: année de référence SDR
* `source1`: valeur textuelle du premier champ d'adresse
* `source2`: valeur textuelle du deuxième champ d'adresse
* `sno_source1`: numéro civique extrait de `source1`
* `sn_norm_source1`: nom de rue normalisé extrait de `source1`
* `sn_norm_target1`: nom de rue de référence normalisé, jugé
  suffisamment similaire à `sn_norm_source1`
* `dir_source1`: direction de la rue extraite de `source1`
* `target1`: adresse complète formée à partir de `sno_source1` et du
  nom de rue de référence (non-normalisé)
* `sim1`: ratio de Levenshtein entre `sn_norm_source1` et `sn_norm_target1`
* `lev1`: distance de Levenshtein entre `sn_norm_source1` et `sn_norm_target1`
* `sn_norm_source2`: nom de rue normalisé extrait de `source2`
* `sn_norm_target2`: nom de rue de référence normalisé, jugé
  suffisamment similaire à `sn_norm_source2`
* `target2`: nom de rue de référence (non-normalisé)
* `sim2`: ratio de Levenshtein entre `sn_norm_source2` et `sn_norm_target2`
* `lev2`: distance de Levenshtein entre `sn_norm_source2` et `sn_norm_target2`
* `lat_addr`: latitude de l'adresse trouvée (à l'aide de `source1`)
* `long_addr`: longitude de l'adresse trouvée (à l'aide de `source1`)
* `lat_inter`: latitude de l'intersection trouvée (à l'aide de `source1` et `source2`)
* `long_inter`: longitude de l'intersection trouvée (à l'aide de `source1` et `source2`)
* `addr_inter_dist`: distance (en mètres) entre l'adresse trouvée (à
  l'aide de `source1`) et l'intersection trouvée (à l'aide de
  `source1` et `source2`)
* `inter_bb_radius`: rayon (en mètres) généré par la "bounding box"
  d'une intersection et son centroide (plus cette valeur est grande,
  plus l'intesection est "ambigue")
