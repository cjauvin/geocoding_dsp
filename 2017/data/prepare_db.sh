#!/bin/bash

set -e

db=${1:-geocoding_dsp}

createdb $db
psql $db -c 'create extension postgis'

##############
# INPUT DATA #
##############

pgdbf -s latin1 T_CJ_ACC.DBF | psql $db

psql $db <<EOF

     alter table t_cj_acc add column rnd real;
     update table t_cj_acc set rnd = random();
     create index on t_cj_acc (rnd);

EOF

#############
# AQ TABLES #
#############

aq_input_srid=3798
aq_output_srid=4269

# -S: lines instead of multilines
shp2pgsql -d -I -s $aq_input_srid:$aq_output_srid -S -W latin1 AQ/AQpro/AQ_ROUTES aq_routes | psql $db

pgdbf -s latin1 AQ/AQpro/AQ_ODONYMES.dbf | psql $db

psql $db <<EOF

   -- aq_routes_full = aq_routes + aq_odonymes

   create table aq_routes_full as
   select r.*, o.odospeci, o.odoorien
   from aq_routes r
   join aq_odonymes o on r.gaseqodo = o.seqodo;

   drop table aq_routes;
   alter table aq_routes_full rename to aq_routes;

   create index on aq_routes (odospeci);
   create index on aq_routes (norte);

EOF

shp2pgsql -d -I -s $aq_input_srid:$aq_output_srid -S -W latin1 AQ/AQgeobati/AQ_ADRESSES aq_adresses | psql $db

pgdbf -s latin1 AQ/AQpro/AQ_REFERENTIEL.dbf | psql $db
pgdbf -s latin1 AQ/AQpro/AQ_MUNICIPALITES.dbf | psql $db

psql $db <<EOF

    -- aq_adresses_full = aq_adresses + {odospeci, odoorien, norte, sdridu_YYYY}

    create table aq_adresses_full
    as select a.*, o.odospeci, o.odoorien, r.norte
    from aq_adresses a
    inner join aq_odonymes o using (seqodo)
    inner join aq_referentiel using (nosqnocivq)
    left join aq_routes r using (idrte);

    drop table aq_adresses;
    alter table aq_adresses_full rename to aq_adresses;

    -- sdridu_YYYY is added below

EOF

###############
# SDR REGIONS #
###############

sdr_buffer_radius=0.01
sdr_srid=4269

for y in 1996 2001 2006 2011; do

    shp2pgsql -d -I -s $sdr_srid -W latin1 ./sdr/$y/sdr${y}_SAAQ sdr_$y | psql $db

    psql $db <<EOF

    -- create buffered SDRs

    drop table if exists sdr_${y}_buffered;
    create table sdr_${y}_buffered as
    select gid, sdridu, st_buffer(geom, $sdr_buffer_radius) as geom from sdr_${y};
    select updategeometrysrid('sdr_${y}_buffered', 'geom', $sdr_srid);
    create index on sdr_${y}_buffered using gist (geom);
    vacuum analyze sdr_${y}_buffered;

    -- create aq_routes / buffered_sdr inter table

    drop table if exists routes_sdr_${y}_buffered_inter;
    create table routes_sdr_${y}_buffered_inter as
    select distinct r.gid, s.sdridu
    from aq_routes r, sdr_${y}_buffered s
    where st_intersects(r.geom, s.geom);

    create index on routes_sdr_${y}_buffered_inter (gid, sdridu);
    vacuum analyze routes_sdr_${y}_buffered_inter;

    -- update aq_adresses table with sdr_YYYY

    alter table aq_adresses add column sdridu_${y} text;
    update aq_adresses a set sdridu_${y} = s.sdridu
    from sdr_${y} s
    where st_contains(s.geom, a.geom);

    create index on aq_adresses (nocivq, odospeci, sdridu_${y});
    create index on aq_adresses (nocivq, norte, sdridu_${y});

EOF

done
