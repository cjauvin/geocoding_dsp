#!/usr/bin/env python
# -*- coding: utf-8 -*-

from __future__ import division
import sys, sqlite3, psycopg2, psycopg2.extras, unicodedata, re, argparse, codecs, locale, operator, cPickle, Levenshtein
from collections import defaultdict
from geocoding import geocode, intersect

# to receive unicode from db
psycopg2.extensions.register_type(psycopg2.extensions.UNICODE)
psycopg2.extensions.register_type(psycopg2.extensions.UNICODEARRAY)

def stripAccents(s):
    return ''.join((c for c in unicodedata.normalize('NFD', s) if unicodedata.category(c) != 'Mn'))

def normalizeString(s):
    return re.sub(r'\W+', ' ', stripAccents(s.upper())).strip()

def normalizeStreetName(sn):
    m = re.match(r'(\d+).*', sn)
    if m: return m.group(1)
    sn = re.sub(r'^DE LA |^DE L |^DE |^DES |^DU |^LE |^L |^LA |^LES |^BD |^BOUL |^AUT ', '', sn)
    sn = re.sub(r'\bSAINT |\bSAINTE |\bST |\bSTE ', '<ST> ', sn)
    return sn

# find numeric token (sno), concat remaining parts (non_sno) and
# return list of (sno, non_sno)'s
def _parseStreetNumber(addr):
    if not addr: return []
    parses = []
    for m in re.finditer('\d+', addr):
        sno = m.group()
        non_sno = (addr[0:m.start()] + addr[m.start()+len(sno):]).strip()
        parses.append((sno, non_sno))
    return parses

def _parseStreetDirections(addr):
    dirs = []
    for d in 'ESN':
        if ' %s ' % d in addr + ' ':
            dirs.append(d)
    if ' W ' in addr + ' ' or ' O ' in addr + ' ':
        dirs.append('O')
        dirs.append('W')
    return dirs

# to pickle a df that would normally use a lambda function: must be named, must be in __main__ namespace
def makeSetDefaultdict():
    return defaultdict(set)

class NgramBasedStreetNameDB:

    def __init__(self):
        # sn_pair: (sn, sn_norm), where sn is DB's streetname field
        self.reg_type_to_reg_val_to_key_to_sn_pairs = {None:{None:defaultdict(set)},
                                                         'ra':defaultdict(makeSetDefaultdict),
                                                         'dr':defaultdict(makeSetDefaultdict),
                                                         'sdr':defaultdict(makeSetDefaultdict)}

    def extract(self, geo_cur, args):
        print 'extracting street name database..',
        sys.stdout.flush()
        self.n = args.n
        #self.use_pos = args.use_pos
        geo_cur.execute("""
select distinct streetname sn, sdridu sdr from aq_routes_2013 str
inner join str_sdr_%s%s_inter inter on str.gid = inter.gid
where streetname is not null
""" % (args.sdr_year, '_buffered' if args.buffered else ''))
        rows = geo_cur.fetchall()
        for row in rows:
            sn_norm = normalizeStreetName(normalizeString(row['sn']))
            sn_processed = False
            for i in range(len(sn_norm)-self.n+1):
                key = sn_norm[i:i+self.n]
                sn_pair = (row['sn'], sn_norm)
                #print sn_pair
                for rt in ['sdr']: #[None, 'ra', 'dr', 'sdr']:
                    self.reg_type_to_reg_val_to_key_to_sn_pairs[rt][row[rt] if rt else None][key].add(sn_pair)
                sn_processed = True
            if not sn_processed:
                key = sn_norm
                sn_pair = (row['sn'], sn_norm)
                for rt in ['sdr']: #[None, 'ra', 'dr', 'sdr']:
                    self.reg_type_to_reg_val_to_key_to_sn_pairs[rt][row[rt] if rt else None][key].add(sn_pair)
        print 'done'

    def query(self, query, regional_constraint):
        rt, rv = regional_constraint
        pairs = set()
        for i in range(len(query)-self.n+1):
            key = query[i:i+self.n]
            pairs |= self.reg_type_to_reg_val_to_key_to_sn_pairs[rt][rv][key]
        if len(query) < self.n:
            key = query
            pairs |= self.reg_type_to_reg_val_to_key_to_sn_pairs[rt][rv][key]
        return pairs

    @classmethod
    def load(cls, fn):
        #print 'loading street name database..',
        print 'loading %s.. ' % fn,
        sys.stdout.flush()
        return cPickle.load(open(fn, 'rb')) # highest protocol, must be read/write in binary mode

    def save(self, fn):
        print 'saving %s.. ' % fn,
        sys.stdout.flush()
        with open(fn, 'wb') as f:
            cPickle.dump(self, f, -1)
        print 'done'

class AddressMatcher:

    def __init__(self, geo_cur, args):
        self.db = NgramBasedStreetNameDB()
        self.db.extract(geo_cur, args)
        self.geo_cur = geo_cur
        self.args = args
        self.n_match_calls = 0
        self.n_match_trials = 0
        self.n_geocoding_cache_hits = 0
        self.geocoding_cache = {} # geocoding: set(sno, sn_target, regional_constraint) -> point
                                  # intersection: set() -> point
        self.inter_table = 'str_sdr_%s%s_inter' % (args.sdr_year , '_buffered' if args.buffered else '')

    def matchStreetNumberAndName(self, addr, regional_constraint, no_route):
        self.n_match_calls += 1
        results = []
        snos = []
        addr_norm = normalizeString(addr)
        dirs = _parseStreetDirections(addr_norm) + ['']
        for sno, non_sno in _parseStreetNumber(addr_norm):
            if sno != no_route:
                snos.append(sno)
            for sn_source, sn_source_norm, sn_target, sn_target_norm in self._streetNameParsingIter(non_sno, regional_constraint):
                sim = Levenshtein.ratio(sn_source_norm, sn_target_norm)
                if sim == 1 or (len(sn_target) >= self.args.exact_match_cutoff and sim >= self.args.sim_threshold):
                    for dir in dirs:
                        self.n_match_trials += 1
                        key = (sno, sn_target, dir, regional_constraint)
                        if key in self.geocoding_cache:
                            addr_geo = self.geocoding_cache[key]
                            self.n_geocoding_cache_hits += 1
                        else:
                            addr_geo = geocode(self.geo_cur, int(sno), sn_target, dir, dict([regional_constraint]), self.inter_table)
                        self.geocoding_cache[key] = addr_geo
                        if addr_geo:
                            target = '%s %s%s' % (sno, sn_target, ' %s' % dir if dir else '')
                            target_source_len_ratio = len(target) / len(addr)
                            x, y = re.match('POINT\((.*) (.*)\)', addr_geo[0]['geo']).groups() if len(addr_geo) == 1 else (None, None)
                            result = (sim, len(addr_geo) > 1, sno, sn_source, sn_source_norm, sn_target, sn_target_norm, target, dir, target_source_len_ratio, x, y)
                            if sim == 1 and len(addr_geo) == 1: # cannot do better, stop search
                                return result
                            results.append(result)
        # no_route processing
        if no_route:
            for sno in snos:
                for dir in dirs:
                    key = (sno, no_route, dir, regional_constraint)
                    if key in self.geocoding_cache:
                        addr_geo = self.geocoding_cache[key]
                    else:
                        addr_geo = geocode(self.geo_cur, int(sno), no_route, dir, dict([regional_constraint]), self.inter_table)
                    self.geocoding_cache[key] = addr_geo
                    if addr_geo:
                        #print 'no_route:', sno, no_route
                        target = '%s %s%s' % (sno, no_route, ' %s' % dir if dir else '')
                        target_source_len_ratio = len(target) / len(addr)
                        x, y = re.match('POINT\((.*) (.*)\)', addr_geo[0]['geo']).groups() if len(addr_geo) == 1 else (None, None)
                        return (1, len(addr_geo) > 1, sno, no_route, no_route, no_route, no_route, target, dir, target_source_len_ratio, x, y)
        if results: return sorted(results, reverse=True)[0]
        return None

    def matchIntersection(self, addr1, addr2, regional_constraint):
        if not addr1 or not addr2: return None
        self.n_match_calls += 1
        results = []
        for sn_source1, sn_source_norm1, sn_target1, sn_target_norm1 in self._streetNameParsingIter(normalizeString(addr1), regional_constraint):
            for sn_source2, sn_source_norm2, sn_target2, sn_target_norm2 in self._streetNameParsingIter(normalizeString(addr2), regional_constraint):
                #print sn_target1, sn_target2
                sim1 = Levenshtein.ratio(sn_source_norm1, sn_target_norm1)
                sim2 = Levenshtein.ratio(sn_source_norm2, sn_target_norm2)
                #print '"%s":"%s" = %s' % (sn_source_norm1, sn_target_norm1, sim1)
                #print '"%s":"%s" = %s' % (sn_source_norm2, sn_target_norm2, sim2)
                if ((len(sn_target1) >= self.args.exact_match_cutoff and sim1 >= self.args.sim_threshold) or sim1 == 1) and \
                       ((len(sn_target2) >= self.args.exact_match_cutoff and sim2 >= self.args.sim_threshold) or sim2 == 1):
                    self.n_match_trials += 1
                    key = (sn_target1, sn_target2, regional_constraint)
                    if key in self.geocoding_cache:
                        inter_geo = self.geocoding_cache[key]
                    else:
                        inter_geo = intersect(self.geo_cur, sn_target1, sn_target2, dict([regional_constraint]), self.args.enforce_single_inter_point, self.inter_table)
                        self.geocoding_cache[key] = inter_geo
                    if inter_geo:
                        target1_source1_len_ratio = len(sn_target1) / len(addr1)
                        target2_source2_len_ratio = len(sn_target2) / len(addr2)
                        x, y = re.match('POINT\((.*) (.*)\)', inter_geo[0]['geo']).groups() if len(inter_geo) == 1 else (None, None)
                        result = ((sim1, sim2), sn_source1, sn_source2, sn_source_norm1, sn_source_norm2, sn_target1, sn_target2, sn_target_norm1, \
                                  sn_target_norm2, target1_source1_len_ratio, target2_source2_len_ratio, x, y)
                        if sim1 == 1 and sim2 == 1: return result
                        results.append(result)
        if results: return sorted(results, reverse=True)[0]
        return None

    def _streetNameParsingIter(self, source, regional_constraint):
        #sn_parts = re.split(r'\W+', source)
        sn_parts = source.split()
        n_sn_parts = len(sn_parts)
        for n in range(n_sn_parts, 0, -1):
            for p in range(n_sn_parts - n + 1):
                sn_source = ' '.join(sn_parts[p:p+n])
                sn_source_norm = normalizeStreetName(sn_source)
                for sn_target, sn_target_norm in self.db.query(sn_source_norm, regional_constraint):
                    yield sn_source, sn_source_norm, sn_target, sn_target_norm

if __name__ == '__main__':

    parser = argparse.ArgumentParser(description='')
    #parser.add_argument('streetname_pkl', help='')
    parser.add_argument('year', help='input year')
    parser.add_argument('--addr', help='')
    parser.add_argument('--inter', help='', nargs=2)
    parser.add_argument('--regional_constraint', nargs=2, default=(None,None))
    #parser.add_argument('--sim_fn', help='similarity function (default ratio)', default='ratio', choices=('ratio','jaro','jaro_winkler'))
    parser.add_argument('--sim_threshold', help='similarity threshold value (default 0.8)', type=float, default=0.8)
    parser.add_argument('--enforce_single_inter_point', help='default False', action='store_true')
    parser.add_argument('--exact_match_cutoff', help='any name smaller than this must match exactly (default 5)', type=int, default=5)
    parser.add_argument('--debug', help='default False', action='store_true')
    extract_group = parser.add_argument_group('streetname DB extraction')
    extract_group.add_argument('--extract', help='will extract a new streetname DB (and store it as a pkl file)', action='store_true', default=False)
    extract_group.add_argument('--ngram', help='ngram order; default 3', type=int, default=3)
    extract_group.add_argument('--use_pos', help='use ngram position; default False', action='store_true')
    try: args = parser.parse_args()
    except IOError, msg: parser.error(str(msg))

    if args.regional_constraint: assert args.regional_constraint[0] in [None,'ra','dr','sdr']
    args.regional_constraint = tuple(args.regional_constraint)
    if args.addr: args.addr = args.addr.decode('utf8')
    if args.inter: args.inter = [s.decode('utf8') for s in args.inter]

    geo_conn = psycopg2.connect("dbname=geocoding_dsp")
    geo_cur = geo_conn.cursor(cursor_factory=psycopg2.extras.RealDictCursor)

    if args.extract:

        db = NgramBasedStreetNameDB()
        db.extract(geo_cur, args)
        db.save('../data/dmti_qc_streets_2008_sdr_%s_ngram=%s_use_pos=%s.pkl' % (args.year, args.ngram, 'yes' if args.use_pos else 'no'))

    else:
        matcher = AddressMatcher(geo_cur, args)
        if args.addr:
            print matcher.matchStreetNumberAndName(args.addr, args.regional_constraint)
        if args.inter:
            print matcher.matchIntersection(args.inter[0], args.inter[1], args.regional_constraint)

    geo_conn.close()
