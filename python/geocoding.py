#!/usr/bin/env python
# -*- coding: utf-8 -*-

from __future__ import division
import sys, re
from math import *

def geocode(cur, sno, sn, direction, sdr, inter_table):
    q = """select st_astext(st_line_interpolate_point(geom,
                case when %%s %%%% 2 = gaminadr %%%% 2 and
                          %%s between least(gaminadr, gamaxadr) and
                                      greatest(gaminadr, gamaxadr)
                          then case when gaminadr = gamaxadr then 0
                               else (%%s - least(gaminadr, gamaxadr)) /
                                    (greatest(gaminadr, gamaxadr) -
                                     least(gaminadr, gamaxadr))::real
                               end
                     when %%s %%%% 2 = drminadr %%%% 2 and
                          %%s between least(drminadr, drmaxadr) and
                                      greatest(drminadr, drmaxadr)
                          then case when drminadr = drmaxadr then 0
                               else (%%s - least(drminadr, drmaxadr)) /
                                    (greatest(drminadr, drmaxadr) -
                                     least(drminadr, drmaxadr))::real
                               end
                end)) as geo, str.gid
            from aq_routes_2013 str, %s inter
            where streetname = %%s and
            ((gaminadr %%%% 2 = %%s %%%% 2 and
              %%s between least(gaminadr, gamaxadr) and
                          greatest(gaminadr, gamaxadr))
                or
             (drminadr %%%% 2 = %%s %%%% 2 and
              %%s between least(drminadr, drmaxadr) and
                          greatest(drminadr, drmaxadr)))
            and str.gid = inter.gid
            and inter.sdridu = %%s
    """ % inter_table
    values = [sno, sno, sno, sno, sno, sno, sn, sno,
              sno, sno, sno, sdr]
    if direction:
        q += ' and str.gaodoorien = %s'
        values += [direction.capitalize()]
    #print cur.mogrify(q, values)
    cur.execute(q, values)
    #return cur.fetchall()
    return {r['gid']: r['geo'] for r in cur.fetchall()} # gid -> 'POINT(.. ..)'

def intersect(cur, sn1, sn2, sdr, enforce_single_inter_point, inter_table):
    if enforce_single_inter_point:
        q = """
        select distinct st_astext(inter) geo from
            (select st_intersection(
                (select st_collect(geom) from aq_routes_2013 str, %s inter
                     where streetname = %%s
                     and str.gid = inter.gid and inter.sdridu = %%s),
                (select st_collect(geom) from aq_routes_2013 str, %s inter
                     where streetname = %%s
                    and str.gid = inter.gid and inter.sdridu = %%s)
            ) inter ) _
            where not st_isempty(inter)
        """ % (inter_table, inter_table)
    else:
        q = """
    select distinct st_astext(st_centroid(inter)) geo from
    (select st_intersection(
        (select st_collect(geom) from aq_routes_2013 str, %s inter
             where streetname = %%s
                and str.gid = inter.gid and inter.sdridu = %%s),
        (select st_collect(geom) from aq_routes_2013 str, %s inter
             where streetname = %%s
                and str.gid = inter.gid and inter.sdridu = %%s)) inter) _
    where not st_isempty(inter)
        """ % (inter_table, inter_table)
    values = [sn1, sdr, sn2, sdr]
    #print cur.mogrify(q, values)
    cur.execute(q, values)
    return cur.fetchall()

def disambiguate_with_intersection(cur, ambiguous_gids, inter_sn,
                                   sdr, inter_table):
    q = """
        select st_isempty(st_intersection(
          (select geom from aq_routes_2013 str
             where gid = %%s),
          (select st_collect(geom) from aq_routes_2013 str,
                                        %s inter
             where streetname = %%s
             --where streetname ilike '%%%%JOLY%%%%'
             and str.gid = inter.gid and inter.sdridu = %%s)))
    """ % (inter_table)
    res = []
    for gid in ambiguous_gids:
        values = [gid, inter_sn, sdr]
        cur.execute(q, values)
        #print cur.mogrify(q, values)
        if cur.fetchone()['st_isempty']:
            res.append(gid)
    return res[0] if len(res) == 1 else None

if __name__ == '__main__':

    import psycopg2, psycopg2.extras
    conn = psycopg2.connect("dbname=geocoding_dsp")
    cur = conn.cursor(cursor_factory=psycopg2.extras.RealDictCursor)
    #print geocode_old(cur, 7067, 'DE LANAUDIÈRE', None, {'sdr':'2466025'})
    #print geocode(cur, 6616, 'PULLMAN')
    #print geocode(cur, 2250, '43')

    #print geocode(cur, 7067, 'DE LANAUDIÈRE', None, '2466023', 'str_sdr_2011_buffered_inter')
    # print intersect(cur, 'JEAN-TALON', 'PAPINEAU', {'sdr':'2466023'}, True,
    #                 'str_sdr_2011_buffered_inter')

    # print geocode(cur, 7067, 'DE LANAUDIÈRE', None, {'sdr':'2466025'},
    #               'str_sdr_1996_buffered_inter')
    # print intersect(cur, 'JEAN-TALON', 'PAPINEAU', {'sdr':'2466025'}, True,
    #                 'str_sdr_1996_buffered_inter')

    #print geocode(cur, 506, 'CHERRIER', None, '2466023', 'str_sdr_2006_buffered_inter')
    #print geocode(cur, 506, 'CHERRIER', None, '2466023', 'str_sdr_2006_buffered_inter')
    #print geocode(cur, 5, 'PAIEMENT', None, '2466023', 'str_sdr_2006_buffered_inter')

    #print disambiguate_with_intersection(cur, [48041, 47625], 'GOUIN', '2466023', 'str_sdr_2006_buffered_inter')
    print disambiguate_with_intersection(cur, [39771, 47694], 'SAINTE-ANNE', '2466023', 'str_sdr_2006_buffered_inter')
