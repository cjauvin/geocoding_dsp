# -*- coding: utf-8 -*-

from __future__ import division, print_function


STR_TABLE = 'aq_routes_2014'
STREETNAME_FIELD = 'gaodospeci'
SRID = 4269


def geocode(cur, sno, sn, direction, sdr, inter_table):

    sn_field = STREETNAME_FIELD
    if isinstance(sn, int):
        sn_field = 'norte'

    assert direction is None or direction.lower() in {'sud', 'ouest', 'nord-est', 'est', 'sud-ouest', 'nord-ouest', 'nord', 'sud-est'}  # noqa

    q = """
           select distinct

           st_astext(st_line_interpolate_point(

               geom,

               case when (%(sno)s %% 2) = (gaminadr %% 2) and %(sno)s between least(gaminadr, gamaxadr) and greatest(gaminadr, gamaxadr)
                    then case when gaminadr = gamaxadr then 0
                              else (%(sno)s - least(gaminadr, gamaxadr)) / (greatest(gaminadr, gamaxadr) - least(gaminadr, gamaxadr))::real
                         end
                    when (%(sno)s %% 2) = (drminadr %% 2) and %(sno)s between least(drminadr, drmaxadr) and greatest(drminadr, drmaxadr)
                    then case when drminadr = drmaxadr then 0
                              else (%(sno)s - least(drminadr, drmaxadr)) / (greatest(drminadr, drmaxadr) - least(drminadr, drmaxadr))::real
                         end
               end

           )) as coords, str.gid

           from {str_table} str, {inter_table} inter

           where {sn_field} = %(sn)s

           and (
                  ((gaminadr %% 2) = (%(sno)s %% 2) and %(sno)s between least(gaminadr, gamaxadr) and greatest(gaminadr, gamaxadr))
                   or
                  ((drminadr %% 2) = (%(sno)s %% 2) and %(sno)s between least(drminadr, drmaxadr) and greatest(drminadr, drmaxadr))
            )

            and str.gid = inter.gid

            and inter.sdridu = %(sdridu)s

            {direction_clause}
    """

    q = q.format(
        str_table=STR_TABLE,
        inter_table=inter_table,
        sn_field=sn_field,
        direction_clause=' and str.gaodoorien = %(direction)s' if direction else ''
    )

    values = {'sno': sno, 'sn': sn, 'sdridu': sdr}
    if direction:
        values['direction'] = direction.capitalize()

    # print(cur.mogrify(q, values))
    cur.execute(q, values)

    # gid -> 'POINT(.. ..)'
    #return {r['gid']: r['coord'] for r in cur.fetchall()}
    return [{'coords': r['coords'], 'gid': r['gid']} for r in cur.fetchall()]


def intersect(cur, sn1, sn2, sdr, inter_table):

    sn1_field = STREETNAME_FIELD
    if isinstance(sn1, int):
        sn1_field = 'norte'
    sn2_field = STREETNAME_FIELD
    if isinstance(sn2, int):
        sn2_field = 'norte'

    q = """
        with p1 as (

            select st_collect(geom) as p1
            from {str_table} str, {inter_table} inter
            where {sn1_field} = %(sn1)s
            and str.gid = inter.gid
            and inter.sdridu = %(sdr)s

        ), p2 as (

            select st_collect(geom) as p2
            from {str_table} str, {inter_table} inter
            where {sn2_field} = %(sn2)s
            and str.gid = inter.gid
            and inter.sdridu = %(sdr)s

        ), inter as (

            select st_intersection(p1, p2) as inter
            from p1, p2

        ), bb as (

            select st_envelope(inter) as bb
            from inter

        ), centroid as (

           select st_centroid(bb) as centroid
           from bb

        ), bb_radius_in_meters as (

           select st_distance(
               centroid::geography,
               st_setsrid(
                   st_makepoint(
                       st_xmin(bb),
                       st_ymin(bb)
                   ),
                   {srid}
               )::geography
           ) as bb_radius_in_meters
           from centroid, bb

        )

        select st_astext(centroid) as centroid,
               bb_radius_in_meters
        from centroid, bb_radius_in_meters
        where centroid is not null
        and bb_radius_in_meters is not null
    """

    q = q.format(
        sn1_field=sn1_field,
        sn2_field=sn2_field,
        str_table=STR_TABLE,
        inter_table=inter_table,
        srid=SRID
    )

    values = {'sn1': sn1, 'sn2': sn2, 'sdr': sdr}
    # print cur.mogrify(q, values)
    cur.execute(q, values)
    row = cur.fetchone()
    return {'coords': row['centroid'],
            'bb_radius': row['bb_radius_in_meters']} if row else {}


def disambiguate_with_intersection(cur, ambiguous_gids, inter_sn, sdr, inter_table):
    sn_field = STREETNAME_FIELD
    if isinstance(inter_sn, int):
        sn_field = 'norte'

    q = """
        select st_isempty(st_intersection(

            (select geom
             from {str_table} str where gid = %(gid)s),

            (select st_collect(geom)
             from {str_table} str, {inter_table} inter
             where {sn_field} = %(inter_sn)s
             and str.gid = inter.gid
             and inter.sdridu = %(sdr)s)

       ))
    """

    q = q.format(
        str_table=STR_TABLE,
        inter_table=inter_table,
        sn_field=sn_field
    )

    res = []
    for gid in ambiguous_gids:
        values = {'gid': gid, 'inter_sn': inter_sn, 'sdr': sdr}
        cur.execute(q, values)
        #print cur.mogrify(q, values)
        if not cur.fetchone()['st_isempty']:
            res.append(gid)
    return res[0] if len(res) == 1 else None


# http://stackoverflow.com/a/2213199/787842
def str_table_has_index_on_column(cur, col):
    q = """
    select %s in
      (select a.attname
        from pg_class t, pg_class i, pg_index ix, pg_attribute a
        where
          t.oid = ix.indrelid
          and i.oid = ix.indexrelid
          and a.attrelid = t.oid
          and a.attnum = ANY(ix.indkey)
          and t.relkind = 'r'
          and t.relname like 'aq_routes_2013') as has_idx;
    """
    cur.execute(q, [col])
    return cur.fetchone()['has_idx']


if __name__ == '__main__':

    import psycopg2
    import psycopg2.extras

    conn = psycopg2.connect("dbname=geocoding_dsp")
    cur = conn.cursor(cursor_factory=psycopg2.extras.RealDictCursor)

    # # single
    print(geocode(cur, 7067, 'De Lanaudière', direction=None, sdr='2466025', inter_table='str_sdr_1996_buffered_inter'))

    # # ambiguous
    print(geocode(cur, 1000, 'Gouin', direction=None, sdr='2466023', inter_table='str_sdr_2011_buffered_inter'))

    print(geocode(cur, 1000, 'Gouin', direction='ouest', sdr='2466023', inter_table='str_sdr_2011_buffered_inter'))

    print(geocode(cur, 1000, 'Gouin', direction='est', sdr='2466023', inter_table='str_sdr_2011_buffered_inter'))

    print(intersect(cur, 'De Lanaudière', 'Jean-Talon', '2466023', 'str_sdr_2011_buffered_inter'))  # noqa
    print(intersect(cur, 'Décarie', 'Sherbrooke', '2466023', 'str_sdr_2011_buffered_inter'))  # noqa

    #print geocode(cur, 506, 'CHERRIER', None, '2466023', 'str_sdr_2006_buffered_inter')
    #print geocode(cur, 506, 'CHERRIER', None, '2466023', 'str_sdr_2006_buffered_inter')
    #print geocode(cur, 5, 'PAIEMENT', None, '2466023', 'str_sdr_2006_buffered_inter')

    #print disambiguate_with_intersection(cur, [48041, 47625], 'GOUIN', '2466023', 'str_sdr_2006_buffered_inter')
    #print disambiguate_with_intersection(cur, [39771, 47694], 'SAINTE-ANNE', '2466023', 'str_sdr_2006_buffered_inter')

    #print geocode(cur, 2, 'CURÉ-LABELLE', None, '2473010', 'str_sdr_2006_buffered_inter')

    #print intersect(cur, '327', 'HARRINGTON', '2476052', True, 'str_sdr_2006_buffered_inter')
    #print intersect(cur, 'LA SALETTE', 'RIVIÈRE-DU-NORD', '2475017', True, 'str_sdr_2006_buffered_inter')
    # print intersect_and_get_bb_radius(cur, 'JEAN-TALON', 'DE LANAUDIÈRE', '2466023', 'str_sdr_2006_buffered_inter')
    #print intersect_and_get_bb_radius(cur, 'DÉCARIE', 'SHERBROOKE', '2466023', 'str_sdr_2006_buffered_inter')

    # print geocode(cur, 2000, '%DU%NORD%', None, '2476043', 'str_sdr_2006_buffered_inter',
    #               sn_op='like', return_by_gid=False)

    # print intersect_and_get_bb_radius(cur, 327, '%JACKSON%', '2476043',
    #                                   'str_sdr_2006_buffered_inter', '=', 'like')

    # print intersect_and_get_bb_radius(cur, '%WILFRID%HAMEL%', '%SACREMENT%', '2423027',
    #                                   'str_sdr_2006_buffered_inter', 'like', 'like')

    # print intersect_and_get_bb_radius(cur, '%WILFRID%', '%PARC%', '2463035',
    #                                   'str_sdr_2006_buffered_inter', 'like', 'like')

    #print str_table_has_index_on_column(cur, 'bla')

    # print intersect_and_get_bb_radius(cur, '%5e%', '%Claire%', '2473035',
    #                                   'str_sdr_2006_buffered_inter', 'ilike', 'ilike')
