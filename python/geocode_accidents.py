#!/usr/bin/env python
# -*- coding: utf-8 -*-

from __future__ import division
import sys, sqlite3, psycopg2, psycopg2.extras, re, codecs, argparse, os.path
from collections import defaultdict
#from dbfpy import dbf
#import dbf
from address_matching import *
import little_pger as pg

# to receive unicode from db
psycopg2.extensions.register_type(psycopg2.extensions.UNICODE)
psycopg2.extensions.register_type(psycopg2.extensions.UNICODEARRAY)

parser = argparse.ArgumentParser(description='')
#parser.add_argument('year', help='input year')
parser.add_argument('input_table', help='')
parser.add_argument('--how_many', help='default all', default='all')
parser.add_argument('--coarsest_regional_constraint',
                    help='regional constraint (default sdr, i.e. the finest)',
                    choices=('sdr','dr','ra'), default='sdr')
parser.add_argument('--sim_threshold',
                    help='similarity threshold value (default 0.70)',
                    type=float, default=0.70)
parser.add_argument('--enforce_single_inter_point',
                    help='default False (i.e. less restrictive)',
                    action='store_true')
parser.add_argument('--exact_match_cutoff',
                    help='any name smaller than this must match exactly (default 5)',
                    type=int, default=5)
parser.add_argument('--report_every', help='default 1000',
                    type=int, default=1000)
parser.add_argument('--n', help='ngram order; default 3',
                    type=int, default=3)
parser.add_argument('--debug', help='default False', action='store_true')
parser.add_argument('--debug_id', help='', default=None)
parser.add_argument('--dont_save', help='default False', action='store_true')
parser.add_argument('--no_buffer', help='default False', action='store_true')
parser.add_argument('--sdr_year')
try: args = parser.parse_args()
except IOError, msg: parser.error(str(msg))

if args.coarsest_regional_constraint == 'sdr':
    regional_constraints = ['sdr']
elif args.coarsest_regional_constraint == 'dr':
    regional_constraints = ['sdr', 'dr']
elif args.coarsest_regional_constraint == 'ra':
    regional_constraints = ['sdr', 'dr', 'ra']

args.how_many = -1 if args.how_many == 'all' else int(args.how_many)
args.buffered = not args.no_buffer
#args.sdr_year = args.sdr_year if args.sdr_year else args.year

geo_conn = psycopg2.connect("dbname=geocoding_dsp")
#geo_conn.set_isolation_level(psycopg2.extensions.ISOLATION_LEVEL_AUTOCOMMIT)
geo_cur = geo_conn.cursor(cursor_factory=psycopg2.extras.RealDictCursor)

matcher = AddressMatcher(geo_cur, args)

if args.debug_id: args.dont_save = True
#if not args.dont_save: result_file = codecs.open(out_fn, 'w', 'utf8')

n_ok = 0
n_total = 0
n_single = 0
n_inter = 0
cache = {} # (source1, source2, ra, dr, sdr) -> result
n_cache_hits = 0

result_fields = [('id', 'int'), ('success', 'bool'), ('match_type', 'text'),
                 ('ra', 'text'), ('dr', 'text'), ('sdr', 'text'),
                 ('found_in', 'text'), ('source1', 'text'), ('source2', 'text'),
                 ('target1', 'text'), ('target2', 'text'), ('sim1', 'float'),
                 ('sim2', 'float'), ('source_norm1', 'text'),
                 ('source_norm2', 'text'), ('target_norm1', 'text'),
                 ('target_norm2', 'text'), ('source1_direction', 'text'),
                 ('target1_source1_len_ratio', 'float'),
                 ('target2_source2_len_ratio', 'float'),
                 ('x', 'float'), ('y', 'float'), ('srid', 'int')]
field_str = ', '.join(['%s %s' % (f, t) for f, t in result_fields])
result_table = args.input_table + '_results'
geo_cur.execute('drop table if exists %s; create table %s ( %s );' %
                (result_table, result_table, field_str))
result_fields = [f for f, t in result_fields]

for row in pg.select(geo_cur, args.input_table):

    n_total += 1
    if n_total % args.report_every == 0 and not args.debug_id: print n_total
    if args.how_many > 0 and n_total >= args.how_many: break

    id, source1, source2, ra, sdr, no_route = [row[f] for f in
                ['num', 'rue_accdn', 'accdn_pres', 'ra', 'sdr', 'no_route']]
    source1 = source1.strip()
    source2 = source2.strip()

    if args.debug_id and args.debug_id != id: continue

    sdr = '24' + sdr
    dr = sdr[:4]
    regions_by_type = {'ra':ra, 'dr':dr, 'sdr':sdr}
    # just to make sure it doesn't meddle with the csv output

    # cache
    if (source1, source2, no_route, ra, dr, sdr) in cache:
        if not args.dont_save:
            values = [id] + cache[(source1, source2, no_route, ra, dr, sdr)]
            pg.insert(geo_cur, result_table,
                      values=dict(zip(result_fields, values)))
        n_ok += 1
        n_cache_hits += 1
        continue

    # single address (found in source1)
    found_in_region_type = None
    for r in regional_constraints:
        result = matcher.matchStreetNumberAndName(source1,
                                                  (r, regions_by_type[r]),
                                                  no_route)
        if result:
            found_in_region_type = r
            break
    if found_in_region_type:
        sim, is_ambiguous, sno, sn_source, sn_source_norm, sn_target, \
        sn_target_norm, target, dir, target_source_len_ratio, x, y = result
        values = [id, 'yes', 'ambiguous_addr' if is_ambiguous else 'single_addr',
                  ra, dr, sdr, found_in_region_type, source1, None,
                  target, None, sim, None, sn_source_norm, None, '%s %s' %
                  (sno, sn_target_norm), None, dir, target_source_len_ratio,
                  None, x, y, 4326]
        pg.insert(geo_cur, result_table, values=dict(zip(result_fields, values)))
        cache[(source1, source2, no_route, ra, dr, sdr)] = values[1:]
        n_ok += 1
        n_single += 1
        continue

    # intersection (source1 + source2)
    found_in_region_type = None
    for r in regional_constraints:
        result = matcher.matchIntersection(source1, source2,
                                           (r, regions_by_type[r]))
        if result:
            found_in_region_type = r
            break
        else:
            for source in [source1, source2]:
                m = re.match('(.*) et (.*)', source, re.I)
                if m:
                    alt_source1, alt_source2 = m.groups()
                    result = matcher.matchIntersection(alt_source1, alt_source2,
                                                       (r, regions_by_type[r]))
                    if result:
                        found_in_region_type = r
                        break
            if found_in_region_type: break
    if found_in_region_type:
        (sim1, sim2), sn_source1, sn_source2, sn_source_norm1, sn_source_norm2, \
        sn_target1, sn_target2, sn_target_norm1, sn_target_norm2, \
        target1_source1_len_ratio, target2_source2_len_ratio, x, y = result
        values = [id, 'yes', 'inter', ra, dr, sdr, found_in_region_type, source1,
                  source2, sn_target1, sn_target2, sim1, sim2,
                  sn_source_norm1, sn_source_norm2, sn_target_norm1,
                  sn_target_norm2, None, target1_source1_len_ratio,
                  target2_source2_len_ratio, x, y, 4326]
        pg.insert(geo_cur, result_table, values=dict(zip(result_fields, values)))
        cache[(source1, source2, no_route, ra, dr, sdr)] = values[1:]
        n_ok += 1
        n_inter += 1
        continue

    # nothing found
    values = [id, 'no', None, ra, dr, sdr, None, source1, source2, None, None,
              None, None, None, None, None, None, None, None, None, None, None,
              4326]
    cache[(source1, source2, no_route, ra, dr, sdr)] = values[1:]
    pg.insert(geo_cur, result_table, values=dict(zip(result_fields, values)))

print '%s/%s (%s%%)' % (n_ok, n_total, n_ok / n_total * 100)
print '%s trials per match call' % (matcher.n_match_trials / matcher.n_match_calls)
#print '%d/%s external/internal cache hits' % (n_cache_hits, matcher.n_geocoding_cache_hits)
print '%d single matches' % n_single
print '%d inter matches' % n_inter

geo_conn.commit()
geo_conn.close()
