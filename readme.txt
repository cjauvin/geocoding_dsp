Algorithme de correction, validation et géocodage des adresses
==============================================================

Christian Jauvin
Février 2012

But de l'algorithme
-------------------

Cet algorithme prend en entrée une série d'adresses (pouvant contenir
des erreurs) provenant de données d'accident, et tente de les
géocoder, pour obtenir des coordonnées géographiques (lat/lon)
permettant de les cartographier de manière exacte. Étant donné les
variations et les erreurs dans l'orthographe des noms de rue, cet
algorithme doit utiliser un mécanisme de recherche qui doit (1) être
efficace et (2) faire preuve d'un certain niveau de tolérance. Pour
cela, nous utilisons trois techniques particulières :

(1) "Pour comparer des pommes avec des pommes" : on normalise tous les
noms de rue traités, ce qui facilite la comparaison en réduisant à
priori certaines variations faciles à détecter (à l'aide d'expressions
régulières simples); p. ex. 'Ste-Catherine' est transformé en '<ST>
CATHERINE', 'De La Loire' en 'LOIRE', etc.

(2) "Pour éviter de comparer des pommes avec des oranges" : on utilise
des n-grammes pour permettre d'établir efficacement une liste de
"candidats potentiels" pour un nom de rue donné, avec lesquels les
comparaisons pourront être faites.

(3) Pour faire les comparaisons, on mesure la "distance d'édition",
qui permet de quantifier la "différence" entre deux chaines de
caractères, en terme de "nombre d'opérations à effectuer" pour
transformer la première en la deuxième; p. ex. 'LOIRE' et 'BOIRE' sont
distants de 1, étant donné qu'il y a une seule lettre à changer pour
passer du 1er au 2ième.

Données de base
---------------

* Street network file DMTI 2008 (shapefile du réseau routier québécois
converti en une table PostGIS : 'qc_streets')

* Shapefiles des régions administratives SDR, DR et RA

* Liste des accidents (plusieurs champs, dont deux d'adresse)

Preprocessing (dictionnaire de noms de rue)
-------------------------------------------

À partir des noms de rue se trouvant dans 'qc_streets', on construit
un dictionnaire, indexé par les différents types de région (SDR, DR,
RA) et par les n-grammes extraits des noms. Par exemple, pour le nom
de rue 'DE LA CÔTE SAINTE-CATHERINE', extrait de 'qc_streets', le
dictionnaire pourrait être organisé de la manière suivante :

sdr    3-gram           nr_norm
100 ->  'COT'  -> 'COTE <ST> CATHERINE'
100 ->  'OTE'  -> 'COTE <ST> CATHERINE'

Les n-grammes sont toutes les séquences de n lettres (avec
chevauchement) se trouvant dans un nom de rue normalisé. Par exemple,
'COT' et 'OTE' sont les deux premiers 3-grammes de 'COTE <ST>
CATHERINE', la forme normalisée du nom 'DE LA CÔTE SAINTE-CATHERINE'.
Les n-grammes servent à rendre efficaces les recherches pour les noms
de rue. Pour un nom de rue contenant une erreur ('CHERBROOKE' au lieu
de 'SHERBROOKE' par exemple), nous chercherons à le comparer aux vrais
noms (ceux extraits de 'qc_streets', donc valides) afin de trouver
celui ayant la plus petite distance d'édition ('SHERBROOKE', dans ce
cas). Le problème c'est qu'il ne serait pas réaliste de tenter tous
les noms de rue possibles (i.e. valides) pour chaque essai. Il faut un
moyen de réduire le nombre de possibilités pour un nom donné. Les
n-grammes permettent d'associer un nom de rue mal orthographié à une
liste (de taille raisonnable) de candidats potentiels, avec qui il
partage au moins un n-gramme particulier (dans notre exemple, tous les
3-grammes sont partagés entre les deux noms, sauf les premiers de
chacun: 'CHE' et 'SHE'). Un autre candidat possible serait par exemple
'SUNNYBROOKE', qui serait plus "loin" que 'SHERBROOKE'). Quand le
dictionnaire est créé, les noms de rue réels (i.e. sans erreur) sont
premièrement décomposés en n-grammes qui leur sont associés. Quand on
fait la correction des adresses, on effectue une décomposition
similaire pour les noms de rue trouvés (contenant potentiellement des
erreurs) pour établir une liste de candidats potentiels avec lesquels
effectuer les comparaisons. Pour diminuer les variations possibles,
les comparaisons sont effectuées à partir des noms normalisés.

Algorithme
----------

Pour chaque "record" d'entrée, on traite:

* Un premier champ d'adresse ('source1') dans lequel on cherchera une
adresse

* Un deuxième champ d'adresse ('source2') avec lequel on cherchera une
intersection (avec 'source1')

* Le SDR (sous-division de recensement) et le DR (division de
recensement)

(1) On commence par chercher une adresse valide dans 'source1'. On y
extrait toutes les séquences numériques contigues pour les traiter en
tant que numéros civiques potentiels. Par exemple :

"près de 123 Roi-René" -> [(#=123, reste='près de Roi-René')]
"face au 100 4ième avenue" -> [(#=100, reste='face au 4ième avenue'),
                               (#=4, reste='face au 100 ième avenue')]

Dans le "reste" du contenu de 'source1' (i.e. ce qui n'est pas une
séquence numérique), on cherche un nom de rue valide, en considérant
tous les sous-groupes de "tokens" (i.e. sous-chaines de caractères
obtenus par "tokenification", i.e. séparées par les caractères
non-alphabétiques) contigus. Par exemple, pour 'Cote Ste-Catherine',
nous considérerons les sous-groupes:

'Cote Ste Catherine' (3 tokens)
'Cote Ste'           (2 tokens)
'Ste Catherine'      (2 tokens)
'Cote'               (1 token)
'Ste'                (1 token)
'Catherine'          (1 token)

en commençant par les sous-groupes de plus grande taille. Pour chaque
sous-groupe, on le décompose tout d'abord en n-grammes pour établir
une liste de candidats potentiels. Pour chaque candidat, on mesure la
distance d'édition des chaines normalisées. Si la distance est sous un
certain seuil, on fera une requête à "qc_streets" pour déterminer si
le géocodage est possible (i.e. le numéro civique extrait est
compatible avec les attributs d'un segment de cette rue
particulière). Cette requête de géocodage est contrainte par la région
polygonale définie par le SDR (i.e. le segment doit se trouver à
l'intérieur de la région pour être valide). Dans certains cas, le
géocodage peut retourner plusieurs résultats : ceux-ci seront
considérés valides mais ambigus. Ceci peut arriver quand la direction
d'une rue (Jean-Talon E/O) n'est pas spécifiée, ou correctement
traitée par l'algorithme. De tous les résultats valides obtenus, celui
ayant la plus faible distance d'édition sera retenu. Lorsque aucun
résultat valide n'est obtenu, on refait une tentative avec le DR, une
région polygonale englobant le SDR, réduisant donc la contrainte de
géocodage (mais augmentant les chances d'un faux positif, ou de
résultats ambigus).

(2) Si aucun résultat n'est trouvé dans le champ 'source1', on
cherchera une intersection valide, formée par deux noms de rue se
trouvant dans les champs 'source1' et 'source2', selon un processus de
recherche similaire. La différence se trouve dans la requête de
géocodage, qui cherche maintenant un point d'intersection entre deux
segments correspondants aux noms de rue trouvés, avec un niveau de
tolérance pour les intersections complexes (p. ex. Maisonneuve et
Décarie), auxquelles ne correspondent pas nécéssairement de points
uniques (on peut utiliser le centroide des points d'intersections
trouvés).

Setup
-----

Pour trouver le SRID d'un shapefile: http://prj2epsg.org/search

Projection originale:
AQ2014_ROUTES: EPSG:3798 (NAD83 / MTQ Lambert)

$ shp2pgsql -d -W latin1 -s 3798:4269 AQ2014_ROUTES | psql geocoding_dsp

$ pgdbf -s latin1 CJsaaq030613141516Accidents20002014.dbf | psql geocoding_dsp
