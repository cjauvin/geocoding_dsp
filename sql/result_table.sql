--drop table if exists cj_saaqbl01at01_20002012fev2014_results_debug;
drop table if exists cjsaaq030613141516accidents20002014_results;

--create table cj_saaqbl01at01_20002012fev2014_results_debug (
create table cjsaaq030613141516accidents20002014_results (
    num int primary key,
    success bool default false,
    result_addr text default 'no',
    result_inter text default 'no',
    sdr text,
    sdr_year text,
    source1 text,
    source2 text,
    sno_source1 int,
    sn_norm_source1 text,
    sn_norm_target1 text,
    dir_source1 text,
    target1 text,
    sim1 float,
    lev1 int,
    sn_norm_source2 text,
    sn_norm_target2 text,
    target2 text,
    sim2 float,
    lev2 int,
    lat_addr float,
    long_addr float,
    lat_inter float,
    long_inter float,
    addr_inter_dist float,
    inter_bb_radius float
--    srid int default 4326
);
