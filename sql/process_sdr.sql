-- correct SDR topological errors
--update :sdr_table set geom = st_multi(st_buffer(geom, 0.0)) where not st_isvalid(geom);

-- create buffered SDRs
drop table if exists :buffered_sdr_table;
create table :buffered_sdr_table as
    select gid, sdridu, st_buffer(geom, 0.01) as geom from :sdr_table;

-- for the quoting trick: http://stackoverflow.com/a/3588796/787842 (in comments)
select updategeometrysrid(:'buffered_sdr_table', 'geom', 4269);

create index :buffered_sdr_table_idx on :buffered_sdr_table using gist (geom);
vacuum analyze :buffered_sdr_table;

-- create inter table
drop table if exists :inter_table;
create table :inter_table as
      select distinct str.gid, sdr.sdridu
      from :str_table str, :buffered_sdr_table sdr
      --where str.geom && sdr.geom and st_intersects(str.geom, sdr.geom);
      where st_intersects(str.geom, sdr.geom);

create index :inter_table_idx on :inter_table (gid, sdridu);
vacuum analyze :inter_table;
