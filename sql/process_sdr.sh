# usage: process_sdr.sh <sdr_year> <aq_routes_year>

if [ "$#" -ne 1 ]; then
    echo "usage: process_sdr.sh <sdr_year>"
    exit
fi

shp2pgsql -d -I -s 4269 -W latin1 ../2017/data/sdr/$1/sdr$1_SAAQ sdr_$1 | psql geocoding_dsp_2017;

psql geocoding_dsp_2017 -f ./process_sdr.sql -v sdr_table=sdr_$1 \
                                      -v buffered_sdr_table=sdr_$1_buffered \
                                      -v buffered_sdr_table_idx=sdr_$1_buffered_gist_idx \
                                      -v inter_table=str_sdr_$1_buffered_inter \
                                      -v inter_table_idx=str_sdr_$1_buffered_idx \
                                      -v str_table=aq_routes ;
