begin;

select probe_geometry_columns();
select dropgeometrycolumn('dmti_qc_mtl_streets_2008', 'the_geom');
drop table if exists dmti_qc_mtl_streets_2008;

create table dmti_qc_mtl_streets_2008 (
 gid        integer              ,
 street     character varying(69) ,
 fromleft    integer               , 
 toleft      integer               , 
 fromright   integer               ,
 toright     integer               , 
 predir      character varying(2)  , 
 pretype     character varying(10) , 
 streetname  character varying(45) , 
 suftype     character varying(10) , 
 sufdir      character varying(2)  , 
 carto       smallint              , 
 left_mun    character varying(70) , 
 right_mun   character varying(70) , 
 left_maf    character varying(70) , 
 right_maf   character varying(70) , 
 left_fsa    character varying(3)  , 
 right_fsa   character varying(3)  , 
 left_prv    character varying(2)  , 
 right_prv   character varying(2)  , 
 uniqueid    integer                
);

select addgeometrycolumn('dmti_qc_mtl_streets_2008', 'the_geom', 4326, 'MULTILINESTRING', 2);

insert into dmti_qc_mtl_streets_2008
select * from dmti_qc_streets_2008 qc_streets
where st_contains((select st_buildarea(st_boundary(st_union(sdr.the_geom)))
                     from sdr_2001 sdr
                     where sdr.sdridu like '2466%'), 
                  qc_streets.the_geom);

commit;
