# -d: drop existing table
# -I: create spatial index
# -S: create simple geometries (i.e. linestrings instead of multi-linestrings)

# 2013:
# shp2pgsql -d -I -S -s 4269 -W latin1 ~/geocoding_dsp/data/geo/aq/AQ_ROUTES2013 aq_routes_2013 | psql geocoding_dsp
# psql geocoding_dsp -c 'create index aq_routes_2013_gaodospeci_idx on aq_routes_2013 (gaodospeci)'
# psql geocoding_dsp -c 'create index aq_routes_2013_norte_idx on aq_routes_2013 (norte)'
# psql geocoding_dsp -c 'create index aq_routes_2013_geom_idx on aq_routes_2013 using gist(geom)'

# # 2014:
# -d: drop table, -I: index on geom, -S: simple geometries
shp2pgsql -d -I -S -s 3798:4269 -W latin1 ~/geocoding_dsp/data/geo/AQ/AQ2014_ROUTES aq_routes_2014 | psql geocoding_dsp
# psql geocoding_dsp -c 'create index aq_routes_2014_gaodospeci_idx on aq_routes_2014 (gaodospeci)'
# psql geocoding_dsp -c 'create index aq_routes_2014_norte_idx on aq_routes_2014 (norte)'
# psql geocoding_dsp -c 'create index aq_routes_2014_geom_idx on aq_routes_2014 using gist(geom)'
